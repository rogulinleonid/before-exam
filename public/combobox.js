function debounce(func, time) {
  let iTimeout = null
  return (...args) => {
    if(iTimeout) {
      window.clearTimeout(iTimeout)
    }
    iTimeout = window.setTimeout(() => {
      func(...args)
      iTimeout = null
    }, time)
  }
}

/* Реализуйте ComboBox и экспортируйте его при помощи export {ComboBox}*/